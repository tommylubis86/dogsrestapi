package com.springboot.DogsRestAPI.Model;

import com.springboot.DogsRestAPI.Model.DogsBreed;
import com.springboot.DogsRestAPI.Model.DogsSubBreed;

import java.io.Serializable;

public class DogsBreedResponse implements Serializable  {

	private DogsBreed[] objBody;

	public DogsBreed[] getObjBody() {
		return objBody;
	}

	public void setObjBody(DogsBreed[] objBody) {
		this.objBody = objBody;
	}
}
