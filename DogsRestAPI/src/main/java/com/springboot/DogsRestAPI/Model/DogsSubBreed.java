package com.springboot.DogsRestAPI.Model;

import java.io.Serializable;

public class DogsSubBreed implements Serializable {
	private String breed;
	private String[] sub_breed;

	public String getBreed() {
		return breed;
	}

	public void setBreed(String breed) {
		this.breed = breed;
	}

	public String[] getSub_breed() {
		return sub_breed;
	}

	public void setSub_breed(String[] sub_breed) {
		if(sub_breed != null) {
			this.sub_breed = sub_breed;
		}
	}
}
