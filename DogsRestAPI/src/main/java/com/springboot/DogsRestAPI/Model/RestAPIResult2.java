package com.springboot.DogsRestAPI.Model;

import java.io.Serializable;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.springboot.DogsRestAPI.Model.Dogs;

public class RestAPIResult2 implements Serializable {
	
	
	private String[] message;
	
	private String status;

	public String[] getMessage() {
		return message;
	}

	public void setMessage(String[] message) {
		this.message = message;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	
	

}
