package com.springboot.DogsRestAPI.Model;

import java.io.Serializable;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.springboot.DogsRestAPI.Model.Dogs;

public class RestAPIResult implements Serializable {
	
	@Autowired
	private Dogs message;
	
	private String status;
	
	public Dogs getMessage() {
		return message;
	}
	public void setMessage(Dogs message) {
		this.message = message;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	

}
