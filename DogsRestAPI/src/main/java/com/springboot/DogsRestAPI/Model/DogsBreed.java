package com.springboot.DogsRestAPI.Model;

import java.io.Serializable;

import com.springboot.DogsRestAPI.Model.DogsSubBreed;

public class DogsBreed implements Serializable {

	private String breed;

	private DogsSubBreed[] sub_breed;

	public String getBreed() {
		return breed;
	}

	public void setBreed(String breed) {
		this.breed = breed;
	}

	public DogsSubBreed[] getSub_breed() {
		return sub_breed;
	}

	public void setSub_breed(DogsSubBreed[] sub_breed) {
		if(sub_breed != null) {
			this.sub_breed = sub_breed;
		}
	}

}
