package com.springboot.DogsRestAPI.Model;

import java.io.Serializable;

public class Dogs implements Serializable {

	private String[] affenpinscher;
	private String[] african;
	private String[] airedale;
	private String[] akita;
	private String[] appenzeller;
	private String[] australian;
	private String[] basenji;
	private String[] beagle;
	private String[] bluetick;
	private String[] borzoi;
	private String[] bouvier;
	private String[] boxer;
	private String[] brabancon;
	private String[] briard;
	private String[] buhund;
	private String[] bulldog;
	private String[] bullterrier;
	private String[] cairn;
	private String[] cattledog;
	private String[] chihuahua;
	private String[] chow;
	private String[] clumber;
	private String[] cockapoo;
	private String[] collie;
	private String[] coonhound;
	private String[] corgi;
	private String[] cotondetulear;
	private String[] dachshund;
	private String[] dalmatian;
	private String[] dane;
	private String[] deerhound;
	private String[] dhole;
	private String[] dingo;
	private String[] doberman;
	private String[] elkhound;
	private String[] entlebucher;
	private String[] eskimo;
	private String[] finnish;
	private String[] frise;
	private String[] germanshepherd;
	private String[] greyhound;
	private String[] groenendael;
	private String[] havanese;
	private String[] hound;
	private String[] husky;
	private String[] keeshond;
	private String[] kelpie;
	private String[] komondor;
	private String[] kuvasz;
	private String[] labrador;
	private String[] leonberg;
	private String[] lhasa;
	private String[] malamute;
	private String[] malinois;
	private String[] maltese;
	private String[] mastiff;
	private String[] mexicanhairless;
	private String[] mix;
	private String[] mountain;
	private String[] newfoundland;
	private String[] otterhound;
	private String[] ovcharka;
	private String[] papillon;
	private String[] pekinese;
	private String[] pembroke;
	private String[] pinscher;
	private String[] pitbull;
	private String[] pointer;
	private String[] pomeranian;
	private String[] poodle;
	private String[] pug;
	private String[] puggle;
	private String[] pyrenees;
	private String[] redbone;
	private String[] retriever;
	private String[] ridgeback;
	private String[] rottweiler;
	private String[] saluki;
	private String[] samoyed;
	private String[] schipperke;
	private String[] schnauzer;
	private String[] setter;
	private String[] sheepdog;
	private String[] shiba;
	private String[] shihtzu;
	private String[] spaniel;
	private String[] springer;
	private String[] stbernard;
	private String[] terrier;
	private String[] vizsla;
	private String[] waterdog;
	private String[] weimaraner;
	private String[] whippet;
	private String[] wolfhound;
	
	public String[] getAffenpinscher() {
		return affenpinscher;
	}
	public void setAffenpinscher(String[] affenpinscher) {
		this.affenpinscher = affenpinscher;
	}
	public String[] getAfrican() {
		return african;
	}
	public void setAfrican(String[] african) {
		this.african = african;
	}
	public String[] getAiredale() {
		return airedale;
	}
	public void setAiredale(String[] airedale) {
		this.airedale = airedale;
	}
	public String[] getAkita() {
		return akita;
	}
	public void setAkita(String[] akita) {
		this.akita = akita;
	}
	public String[] getAppenzeller() {
		return appenzeller;
	}
	public void setAppenzeller(String[] appenzeller) {
		this.appenzeller = appenzeller;
	}
	public String[] getAustralian() {
		return australian;
	}
	public void setAustralian(String[] australian) {
		this.australian = australian;
	}
	public String[] getBasenji() {
		return basenji;
	}
	public void setBasenji(String[] basenji) {
		this.basenji = basenji;
	}
	public String[] getBeagle() {
		return beagle;
	}
	public void setBeagle(String[] beagle) {
		this.beagle = beagle;
	}
	public String[] getBluetick() {
		return bluetick;
	}
	public void setBluetick(String[] bluetick) {
		this.bluetick = bluetick;
	}
	public String[] getBorzoi() {
		return borzoi;
	}
	public void setBorzoi(String[] borzoi) {
		this.borzoi = borzoi;
	}
	public String[] getBouvier() {
		return bouvier;
	}
	public void setBouvier(String[] bouvier) {
		this.bouvier = bouvier;
	}
	public String[] getBoxer() {
		return boxer;
	}
	public void setBoxer(String[] boxer) {
		this.boxer = boxer;
	}
	public String[] getBrabancon() {
		return brabancon;
	}
	public void setBrabancon(String[] brabancon) {
		this.brabancon = brabancon;
	}
	public String[] getBriard() {
		return briard;
	}
	public void setBriard(String[] briard) {
		this.briard = briard;
	}
	public String[] getBuhund() {
		return buhund;
	}
	public void setBuhund(String[] buhund) {
		this.buhund = buhund;
	}
	public String[] getBulldog() {
		return bulldog;
	}
	public void setBulldog(String[] bulldog) {
		this.bulldog = bulldog;
	}
	public String[] getBullterrier() {
		return bullterrier;
	}
	public void setBullterrier(String[] bullterrier) {
		this.bullterrier = bullterrier;
	}
	public String[] getCairn() {
		return cairn;
	}
	public void setCairn(String[] cairn) {
		this.cairn = cairn;
	}
	public String[] getCattledog() {
		return cattledog;
	}
	public void setCattledog(String[] cattledog) {
		this.cattledog = cattledog;
	}
	public String[] getChihuahua() {
		return chihuahua;
	}
	public void setChihuahua(String[] chihuahua) {
		this.chihuahua = chihuahua;
	}
	public String[] getChow() {
		return chow;
	}
	public void setChow(String[] chow) {
		this.chow = chow;
	}
	public String[] getClumber() {
		return clumber;
	}
	public void setClumber(String[] clumber) {
		this.clumber = clumber;
	}
	public String[] getCockapoo() {
		return cockapoo;
	}
	public void setCockapoo(String[] cockapoo) {
		this.cockapoo = cockapoo;
	}
	public String[] getCollie() {
		return collie;
	}
	public void setCollie(String[] collie) {
		this.collie = collie;
	}
	public String[] getCoonhound() {
		return coonhound;
	}
	public void setCoonhound(String[] coonhound) {
		this.coonhound = coonhound;
	}
	public String[] getCorgi() {
		return corgi;
	}
	public void setCorgi(String[] corgi) {
		this.corgi = corgi;
	}
	public String[] getCotondetulear() {
		return cotondetulear;
	}
	public void setCotondetulear(String[] cotondetulear) {
		this.cotondetulear = cotondetulear;
	}
	public String[] getDachshund() {
		return dachshund;
	}
	public void setDachshund(String[] dachshund) {
		this.dachshund = dachshund;
	}
	public String[] getDalmatian() {
		return dalmatian;
	}
	public void setDalmatian(String[] dalmatian) {
		this.dalmatian = dalmatian;
	}
	public String[] getDane() {
		return dane;
	}
	public void setDane(String[] dane) {
		this.dane = dane;
	}
	public String[] getDeerhound() {
		return deerhound;
	}
	public void setDeerhound(String[] deerhound) {
		this.deerhound = deerhound;
	}
	public String[] getDhole() {
		return dhole;
	}
	public void setDhole(String[] dhole) {
		this.dhole = dhole;
	}
	public String[] getDingo() {
		return dingo;
	}
	public void setDingo(String[] dingo) {
		this.dingo = dingo;
	}
	public String[] getDoberman() {
		return doberman;
	}
	public void setDoberman(String[] doberman) {
		this.doberman = doberman;
	}
	public String[] getElkhound() {
		return elkhound;
	}
	public void setElkhound(String[] elkhound) {
		this.elkhound = elkhound;
	}
	public String[] getEntlebucher() {
		return entlebucher;
	}
	public void setEntlebucher(String[] entlebucher) {
		this.entlebucher = entlebucher;
	}
	public String[] getEskimo() {
		return eskimo;
	}
	public void setEskimo(String[] eskimo) {
		this.eskimo = eskimo;
	}
	public String[] getFinnish() {
		return finnish;
	}
	public void setFinnish(String[] finnish) {
		this.finnish = finnish;
	}
	public String[] getFrise() {
		return frise;
	}
	public void setFrise(String[] frise) {
		this.frise = frise;
	}
	public String[] getGermanshepherd() {
		return germanshepherd;
	}
	public void setGermanshepherd(String[] germanshepherd) {
		this.germanshepherd = germanshepherd;
	}
	public String[] getGreyhound() {
		return greyhound;
	}
	public void setGreyhound(String[] greyhound) {
		this.greyhound = greyhound;
	}
	public String[] getGroenendael() {
		return groenendael;
	}
	public void setGroenendael(String[] groenendael) {
		this.groenendael = groenendael;
	}
	public String[] getHavanese() {
		return havanese;
	}
	public void setHavanese(String[] havanese) {
		this.havanese = havanese;
	}
	public String[] getHound() {
		return hound;
	}
	public void setHound(String[] hound) {
		this.hound = hound;
	}
	public String[] getHusky() {
		return husky;
	}
	public void setHusky(String[] husky) {
		this.husky = husky;
	}
	public String[] getKeeshond() {
		return keeshond;
	}
	public void setKeeshond(String[] keeshond) {
		this.keeshond = keeshond;
	}
	public String[] getKelpie() {
		return kelpie;
	}
	public void setKelpie(String[] kelpie) {
		this.kelpie = kelpie;
	}
	public String[] getKomondor() {
		return komondor;
	}
	public void setKomondor(String[] komondor) {
		this.komondor = komondor;
	}
	public String[] getKuvasz() {
		return kuvasz;
	}
	public void setKuvasz(String[] kuvasz) {
		this.kuvasz = kuvasz;
	}
	public String[] getLabrador() {
		return labrador;
	}
	public void setLabrador(String[] labrador) {
		this.labrador = labrador;
	}
	public String[] getLeonberg() {
		return leonberg;
	}
	public void setLeonberg(String[] leonberg) {
		this.leonberg = leonberg;
	}
	public String[] getLhasa() {
		return lhasa;
	}
	public void setLhasa(String[] lhasa) {
		this.lhasa = lhasa;
	}
	public String[] getMalamute() {
		return malamute;
	}
	public void setMalamute(String[] malamute) {
		this.malamute = malamute;
	}
	public String[] getMalinois() {
		return malinois;
	}
	public void setMalinois(String[] malinois) {
		this.malinois = malinois;
	}
	public String[] getMaltese() {
		return maltese;
	}
	public void setMaltese(String[] maltese) {
		this.maltese = maltese;
	}
	public String[] getMastiff() {
		return mastiff;
	}
	public void setMastiff(String[] mastiff) {
		this.mastiff = mastiff;
	}
	public String[] getMexicanhairless() {
		return mexicanhairless;
	}
	public void setMexicanhairless(String[] mexicanhairless) {
		this.mexicanhairless = mexicanhairless;
	}
	public String[] getMix() {
		return mix;
	}
	public void setMix(String[] mix) {
		this.mix = mix;
	}
	public String[] getMountain() {
		return mountain;
	}
	public void setMountain(String[] mountain) {
		this.mountain = mountain;
	}
	public String[] getNewfoundland() {
		return newfoundland;
	}
	public void setNewfoundland(String[] newfoundland) {
		this.newfoundland = newfoundland;
	}
	public String[] getOtterhound() {
		return otterhound;
	}
	public void setOtterhound(String[] otterhound) {
		this.otterhound = otterhound;
	}
	public String[] getOvcharka() {
		return ovcharka;
	}
	public void setOvcharka(String[] ovcharka) {
		this.ovcharka = ovcharka;
	}
	public String[] getPapillon() {
		return papillon;
	}
	public void setPapillon(String[] papillon) {
		this.papillon = papillon;
	}
	public String[] getPekinese() {
		return pekinese;
	}
	public void setPekinese(String[] pekinese) {
		this.pekinese = pekinese;
	}
	public String[] getPembroke() {
		return pembroke;
	}
	public void setPembroke(String[] pembroke) {
		this.pembroke = pembroke;
	}
	public String[] getPinscher() {
		return pinscher;
	}
	public void setPinscher(String[] pinscher) {
		this.pinscher = pinscher;
	}
	public String[] getPitbull() {
		return pitbull;
	}
	public void setPitbull(String[] pitbull) {
		this.pitbull = pitbull;
	}
	public String[] getPointer() {
		return pointer;
	}
	public void setPointer(String[] pointer) {
		this.pointer = pointer;
	}
	public String[] getPomeranian() {
		return pomeranian;
	}
	public void setPomeranian(String[] pomeranian) {
		this.pomeranian = pomeranian;
	}
	public String[] getPoodle() {
		return poodle;
	}
	public void setPoodle(String[] poodle) {
		this.poodle = poodle;
	}
	public String[] getPug() {
		return pug;
	}
	public void setPug(String[] pug) {
		this.pug = pug;
	}
	public String[] getPuggle() {
		return puggle;
	}
	public void setPuggle(String[] puggle) {
		this.puggle = puggle;
	}
	public String[] getPyrenees() {
		return pyrenees;
	}
	public void setPyrenees(String[] pyrenees) {
		this.pyrenees = pyrenees;
	}
	public String[] getRedbone() {
		return redbone;
	}
	public void setRedbone(String[] redbone) {
		this.redbone = redbone;
	}
	public String[] getRetriever() {
		return retriever;
	}
	public void setRetriever(String[] retriever) {
		this.retriever = retriever;
	}
	public String[] getRidgeback() {
		return ridgeback;
	}
	public void setRidgeback(String[] ridgeback) {
		this.ridgeback = ridgeback;
	}
	public String[] getRottweiler() {
		return rottweiler;
	}
	public void setRottweiler(String[] rottweiler) {
		this.rottweiler = rottweiler;
	}
	public String[] getSaluki() {
		return saluki;
	}
	public void setSaluki(String[] saluki) {
		this.saluki = saluki;
	}
	public String[] getSamoyed() {
		return samoyed;
	}
	public void setSamoyed(String[] samoyed) {
		this.samoyed = samoyed;
	}
	public String[] getSchipperke() {
		return schipperke;
	}
	public void setSchipperke(String[] schipperke) {
		this.schipperke = schipperke;
	}
	public String[] getSchnauzer() {
		return schnauzer;
	}
	public void setSchnauzer(String[] schnauzer) {
		this.schnauzer = schnauzer;
	}
	public String[] getSetter() {
		return setter;
	}
	public void setSetter(String[] setter) {
		this.setter = setter;
	}
	public String[] getSheepdog() {
		return sheepdog;
	}
	public void setSheepdog(String[] sheepdog) {
		this.sheepdog = sheepdog;
	}
	public String[] getShiba() {
		return shiba;
	}
	public void setShiba(String[] shiba) {
		this.shiba = shiba;
	}
	public String[] getShihtzu() {
		return shihtzu;
	}
	public void setShihtzu(String[] shihtzu) {
		this.shihtzu = shihtzu;
	}
	public String[] getSpaniel() {
		return spaniel;
	}
	public void setSpaniel(String[] spaniel) {
		this.spaniel = spaniel;
	}
	public String[] getSpringer() {
		return springer;
	}
	public void setSpringer(String[] springer) {
		this.springer = springer;
	}
	public String[] getStbernard() {
		return stbernard;
	}
	public void setStbernard(String[] stbernard) {
		this.stbernard = stbernard;
	}
	public String[] getTerrier() {
		return terrier;
	}
	public void setTerrier(String[] terrier) {
		this.terrier = terrier;
	}
	public String[] getVizsla() {
		return vizsla;
	}
	public void setVizsla(String[] vizsla) {
		this.vizsla = vizsla;
	}
	public String[] getWaterdog() {
		return waterdog;
	}
	public void setWaterdog(String[] waterdog) {
		this.waterdog = waterdog;
	}
	public String[] getWeimaraner() {
		return weimaraner;
	}
	public void setWeimaraner(String[] weimaraner) {
		this.weimaraner = weimaraner;
	}
	public String[] getWhippet() {
		return whippet;
	}
	public void setWhippet(String[] whippet) {
		this.whippet = whippet;
	}
	public String[] getWolfhound() {
		return wolfhound;
	}
	public void setWolfhound(String[] wolfhound) {
		this.wolfhound = wolfhound;
	}
	
	
	

}
