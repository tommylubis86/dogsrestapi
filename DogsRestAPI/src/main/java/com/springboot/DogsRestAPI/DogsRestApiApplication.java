package com.springboot.DogsRestAPI;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DogsRestApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(DogsRestApiApplication.class, args);
	}

}
