package com.springboot.DogsRestAPI.Controller;

import java.io.IOException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.*;
import java.util.HashMap;
import java.util.Map;
import java.util.ArrayList;
import java.util.List;

import com.springboot.DogsRestAPI.Model.RestAPIResult;
import com.springboot.DogsRestAPI.Model.RestAPIResult2;
import com.springboot.DogsRestAPI.Model.DogsBreed;
import com.springboot.DogsRestAPI.Model.DogsBreedDetail;
import com.springboot.DogsRestAPI.Model.DogsSubBreed;
import com.springboot.DogsRestAPI.Model.DogsBreedResponse;

@Controller
public class GetDogsController {

	private final String api_url = "https://dog.ceo/api/breeds/list/all";

	@RequestMapping(value = { "/dogs","/dogs/{dogs_breed}" },
						    method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> getDogs(@PathVariable(name = "dogs_breed", required = false) String paramBreeds) {

		System.out.println("parameter = " + paramBreeds);

		Map map = new HashMap();

		DogsBreed[] arrayDogs = new DogsBreed[10];

		DogsBreed itemDogsBreed;
		DogsBreedDetail searchedDogsBreed;
		DogsSubBreed itemDogsSubBreed;
		DogsBreedResponse objBreedResponse = new DogsBreedResponse();
		

		searchedDogsBreed = new DogsBreedDetail();
		
		RestTemplate restClient = new RestTemplate();

		String requestResult = restClient.getForObject(api_url, String.class);

		ObjectMapper mapper = new ObjectMapper();

		RestAPIResult objResult = new RestAPIResult();
		try {
			objResult = mapper.readValue(requestResult, RestAPIResult.class);
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (JsonProcessingException e) {			
			e.printStackTrace();
		}

		try {
			String strJsonDogs = mapper.writeValueAsString(objResult.getMessage());
			Map<String, String[]> dogsMap = mapper.readValue(strJsonDogs, new TypeReference<Map<String, String[]>>() {
			});
			
			

			if (paramBreeds != null) {
				String[] tempBreeds = dogsMap.get(paramBreeds);
				searchedDogsBreed = new DogsBreedDetail();
				
					if ( (tempBreeds == null) || (tempBreeds.length == 0)) {
						searchedDogsBreed.setBreed(paramBreeds);
						searchedDogsBreed.setSub_breed(null);
					} else {
						DogsSubBreed[] arraySubBreed = new DogsSubBreed[tempBreeds.length];
						searchedDogsBreed.setBreed(paramBreeds);
						for (int x = 0; x < tempBreeds.length; x++) {
							itemDogsSubBreed = new DogsSubBreed();
							itemDogsSubBreed.setBreed(tempBreeds[x]);
							itemDogsSubBreed.setSub_breed(null);
							arraySubBreed[x] = itemDogsSubBreed;
						}
						searchedDogsBreed.setSub_breed(arraySubBreed);
						searchedDogsBreed.setImages(
													getDogsImagesUrl(paramBreeds)
													);
					}
				
			} else {				
					int sizeElemet = dogsMap.size();
					arrayDogs = new DogsBreed[sizeElemet];
					int indeks = 0;
					Set set = dogsMap.entrySet();
					Iterator itr = set.iterator();
					
					while (itr.hasNext()) {
						Map.Entry entry = (Map.Entry) itr.next();
						String entryKey = (String) entry.getKey();
						String[] entryValue = (String[]) entry.getValue();
	
						if (entryValue.length > 0) {
							itemDogsBreed = new DogsBreed();
							
							DogsSubBreed[] arraySubBreed = new DogsSubBreed[entryValue.length];
							itemDogsBreed.setBreed(entryKey);
							
							for (int x = 0; x < entryValue.length; x++) {
								itemDogsSubBreed = new DogsSubBreed();
								itemDogsSubBreed.setBreed(entryValue[x]);
								itemDogsSubBreed.setSub_breed(null);
								arraySubBreed[x] = itemDogsSubBreed;
							}
							itemDogsBreed.setSub_breed(arraySubBreed);
						} else {
							itemDogsBreed = new DogsBreed();
							itemDogsBreed.setBreed(entryKey);
							itemDogsBreed.setSub_breed(null);
						}
						arrayDogs[indeks] = itemDogsBreed;
						indeks++;
					}
					objBreedResponse.setObjBody(arrayDogs);

			}
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}

		

		String jsonResponse = "";
		
		if(paramBreeds == null) {
			try {
				jsonResponse = mapper.writeValueAsString(objBreedResponse.getObjBody());
			} catch (JsonProcessingException e) {
				e.printStackTrace();
			}
		} else { 
			try {
				jsonResponse = mapper.writeValueAsString(searchedDogsBreed);
			} catch (JsonProcessingException e) {
				e.printStackTrace();
			}
		}

		jsonResponse = jsonResponse.replace("null", "[]");
		System.out.println("--------------------------------");
		System.out.println(jsonResponse);
		System.out.println("--------------------------------");

		return new ResponseEntity<String>(jsonResponse, HttpStatus.OK);
	}
	
	
	
	private String[] getDogsImagesUrl(String breedName)
	{
	    String breedDetailURL = "https://dog.ceo/api/breed/" + breedName + "/images";
	    
	    RestTemplate restClient = new RestTemplate();

		String requestResult = restClient.getForObject(breedDetailURL, String.class);
		
		ObjectMapper mapper = new ObjectMapper();

		RestAPIResult2 objResult = new RestAPIResult2();
		
		try {
			objResult = mapper.readValue(requestResult, RestAPIResult2.class);
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (JsonProcessingException e) {			
			e.printStackTrace();
		}
		
		String[] arrayStringImageURL = objResult.getMessage();
		
		
		return arrayStringImageURL;
	}
}


